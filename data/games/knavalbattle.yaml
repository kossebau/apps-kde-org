howto: |
  **Objective:** *Sink all of the opponent’s ships before the opponent sink all the ships of your own.*
  
  If you want to play Naval Battle, you will need two players, either play against the computer or in a network against another player. To play against your computer, first select the difficulty level on the right of the status bar, and then select **Single player** on the welcome screen, or directly on the **Game** menu.

  To start a network game, one player has to host the game by selecting **Host network game** on the welcome screen, or choosing **Game->Host Game...**. A dialog box opens which asks for a **Nickname:** and **Port:**. Normally, Naval Battle will suggest your full name, but you can enter any string you want. The predefined port should be ok. However, if you encounter problems, you can choose any other free port above 1024.
  
  **Note:** *You need to tell the other player in case you use a port other than the default as both players need to use the same port in order to be able to establish a connection.*

  The other player has to choose **Connect to network game**, or click **Game->Connect to Game...**. Again, a **Nickname:** is suggested, but you can choose any name you like. In the field **Hostname:** you have to enter the host name of the server (the machine of the player that initiated the game).

  When you are done, you can start the game. Simply follow the instructions in the statusbar. It will issue hints and suggest what to do next. When you now look at the screen, you will find two grid fields, the so-called "battle areas". The left area belongs to you. This is where you place your ships and where you can follow the military actions of your enemy. The right area is where your enemy's fleet is located. When it's your turn to fire, you need to click on a certain sector (a field of the battle area) where you suppose the ships to be located.

  First, you need to place your ships. The game initiator starts. When he/she is done, player two sets his/her ships.

  Ship placement is very easy: simply click on the field where you want to place your ships. You have four ships to place: the first one will have a length of one square, the next will be two squares long etc. Click on the field where you want to start the placement. By default, ships will be placed horizontally; to rotate them 90 degrees, click the **Right Mouse Button before the placement**.

  When a ship is hit, fire will break out on it. To sink it, hit all the squares it occupies. A sunk ship will appear faded.

  Now you can use the **Left Mouse Button** to fire on the battle area of your enemy. The status bar indicates who is about to shoot.

  You can restart the game by choosing **Game->Restart Game**, or by pressing F5.

  The first player to destroy all their opponents ships wins the game.
