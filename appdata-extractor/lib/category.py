# SPDX-FileCopyrightText: 2017-2018 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import os

# sitter: This is technically codified in /xdg/menus/kf5-applications.menu, but I
# have no parser for that, and breaking it down to it's primary category map is
# a lot more work anyway.
CATEGORY_DESKTOPS_MAP = {
    'AudioVideo': 'kf5-multimedia.directory',
    'Audio': 'kf5-multimedia.directory',
    'Video': 'kf5-multimedia.directory',
    'Development': 'kf5-development.directory',
    'Education': 'kf5-education.directory',
    'Game': 'kf5-games.directory',
    'Graphics': 'kf5-graphics.directory',
    'Network': 'kf5-internet.directory',
    'Office': 'kf5-office.directory',
    'Settings': 'kf5-settingsmenu.directory',
    'System': 'kf5-system.directory',
    'Utility': 'kf5-utilities.directory'
}
MAIN_CATEGORIES = list(CATEGORY_DESKTOPS_MAP.keys())
SUBCATEGORY_DESKTOPS_MAP = {
    # Games
    'ArcadeGame': 'kf5-games-arcade.directory',
    'BoardGame': 'kf5-games-board.directory',
    'CardGame': 'kf5-games-card.directory',
    'KidsGame': 'kf5-games-kids.directory',
    'LogicGame': 'kf5-games-logic.directory',
    'StrategyGame': 'kf5-games-strategy.directory',
    # Education
    'Languages': 'kf5-edu-languages.directory',
    'Math': 'kf5-edu-mathematics.directory',
    'Science': 'kf5-edu-science.directory',
    'Teaching': 'kf5-edu-tools.directory',  # why does this have the 'applications-other' icon?
    'X-KDE-Edu-Misc': 'kf5-edu-miscellaneous.directory',  # this is 'Others'
    # Utilities
    'Accessibility': 'kf5-utilities-accessibility.directory',
    'X-KDE-Utilities-Desktop': 'kf5-utilities-desktop.directory',
    'X-KDE-Utilities-File': 'kf5-utilities-file.directory',
    'X-KDE-Utilities-Peripherals': 'kf5-utilities-peripherals.directory',
    'X-KDE-Utilities-PIM': 'kf5-utilities-pim.directory',
    'XUtilities': 'kf5-utilities-xutils.directory'
}
MAIN_SUBCATEGORIES = list(SUBCATEGORY_DESKTOPS_MAP.keys())


def to_code(category: str):
    desktop = CATEGORY_DESKTOPS_MAP.get(category, SUBCATEGORY_DESKTOPS_MAP.get(category, ''))
    parts = os.path.splitext(desktop)[0].split('-')[1:]
    if parts[0] == 'edu':
        parts[0] = 'education'
    code = '-'.join(parts)
    return code
