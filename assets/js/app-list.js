/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 * SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const txtFilter = document.getElementById('txtFilter');
const catFilter = document.getElementById('catFilter');
const pfFilter = document.getElementById('pfFilter');

const dataArrSplit = /[",]+/;

const checkOne = (regex, category, platform, app) => {
  const checkOneText = () => {
    if (!regex) {
      return true;
    }
    const imgTitleContainsRegex = regex.test(app.querySelector('img').title.replace(/\s/g, ''));
    const descriptionContainsRegex = regex.test(app.querySelector('p').innerText.replace(/\s/g, ''));
    return (imgTitleContainsRegex || descriptionContainsRegex);
  };

  const checkOneCategory = () => {
    if (category == 'all') {
      return true;
    }
    let categories = app.dataset['categories'].slice(2,-2).split(dataArrSplit);
    return (categories.indexOf(category) > -1);
  };

  const checkOnePlatform = () => {
    if (platform == 'all') {
      return true;
    }
    let platforms = app.dataset['platforms'].slice(2,-2).split(dataArrSplit);
    return (platforms.indexOf(platform) > -1);
  };

  return (checkOneText() && checkOneCategory() && checkOnePlatform());
}

const filter = (text, category, platform) => {
  const apps = document.querySelectorAll('.app');
  const regex = text? new RegExp(text.replace(/\s/g, ''), 'i') : null;
  apps.forEach(function(app) {
    if (checkOne(regex, category, platform, app)) {
      // app is kept
      app.classList.remove('d-none');
    } else {
      app.classList.add('d-none');
    }
  });
}

if (txtFilter) {
  txtFilter.addEventListener('input', (event) => filter(event.target.value, catFilter.value, pfFilter.value));
}

if (catFilter) {
  catFilter.addEventListener('change', (event) => filter(txtFilter.value, event.target.value, pfFilter.value));
}

if (pfFilter) {
  pfFilter.addEventListener('change', (event) => filter(txtFilter.value, catFilter.value, event.target.value));
}

const container = document.getElementById('all-apps');

const sortWithSelection = (criteria, direction) => {
  const key = (a) => a.dataset[criteria];
  const compare = (a,b) => {
    if (criteria === 'popularity') {
      const popDiff = parseFloat(key(a)) - parseFloat(key(b));
      return (popDiff != 0? popDiff : a.dataset['name'].localeCompare(b.dataset['name']));
    } else
      return key(a).localeCompare(key(b));
  }

  Array.from(container.children)
  .sort((a, b) => direction == '0'? compare(a,b) : compare(b,a))
  .forEach(child => container.appendChild(child));
}

const sort = document.getElementById('sort');
if (sort) {
  sort.addEventListener('change', (event) => {
    const [criteria, direction] = event.target.value.split('-');
    sortWithSelection(criteria, direction);
  });
}

// when reloading, form controls keep their states, so apply these states to the list
window.addEventListener('DOMContentLoaded', (event) => {
  const [criteria, direction] = sort.value.split('-');
  sortWithSelection(criteria, direction);
  filter(txtFilter.value, catFilter.value, pfFilter.value);
});
