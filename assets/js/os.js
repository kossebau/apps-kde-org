function getOs() {
    let ua = window.navigator.userAgent,
        p = window.navigator.platform,
        os = 'linux';
    /*if (p.indexOf('Mac') == 0) {
        os = 'mac';
    } else */if (p.indexOf('iP') == 0) {
        os = 'ios';
    } else if (p.indexOf('Win') == 0) {
        os = 'windows';
    } else if (/Android/.test(ua)) {
        os = 'android';
    }
    return os;
}

let compatBadges = document.getElementsByClassName(getOs() + '-badge');
if (compatBadges.length == 0) {
    compatBadges = document.getElementsByClassName('linux-badge');
}
document.getElementById('top-badge').innerHTML = compatBadges[0].firstElementChild.outerHTML;
